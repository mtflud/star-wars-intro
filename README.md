# Star Wars Intro Reproduction #

This repo was made as an exercise to replicate the famous Star Wars intro, using technologies like CSS, Javascript and HTML.

## Requirements

It is presumed that you have NodeJS and NPM setted up and running on your computer with the following versions:

`NodeJS >= 8.0`

`NPM >= 5.0`

## Install

To install, simply follow these instructions:

- Clone this repository on your computer
`git clone git@bitbucket.org:mtflud/star-wars-intro.git`
- Navigate to the repo's root folder
`cd star-wars-intro`
- Install dependencies using npm
`npm i`
- Run webpack bundler to compile Javascript
`npm run production`
- Once finished, simply open `index.html` with your browser.

## Demo
An online demo of this repo is available on the following URL:

[Demo](https://starwars.vergil.tech)

## Disclaimers
- This version of the code was tested with all major browsers on their latest versions. It is not intended to work on legacy browsers.
- This exercise could be done using only CSS, but I wanted to include VueJS for evaluating purposes. *I am well aware using VueJS for a project like this is completely overkill* 