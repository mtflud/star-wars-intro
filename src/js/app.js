import './bootstrap'

let musicTheme = new Audio('/dist/audio/star_wars_opening.m4a');

/*
 * Main VueJS Component
 */
window.app = new Vue({
    // :: Bind Vue to element with ID 'app'
    el: '#app',
    data() {
        return {
            introTextVisible: false,
            starsVisible: false,
            logoVisible: false,
            slidingTextVisible: false,
            creditsVisible: false
        }
    },
    mounted() {
        this.startAnimation()
    },
    methods: {
        // :: Wait for n milliseconds
        wait(milliseconds) {
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                    return resolve();
                }, milliseconds);
            });
        },
        // :: Start animating the Star Wars Intro
        async startAnimation() {
            // :: Wait for 1 second before starting, reset audio and hide credits as well.
            musicTheme.currentTime = 0
            this.creditsVisible = false
            await this.wait(1000)
            // :: Fist, show the intro text for 4 seconds with fades...
            await this.animateIntroText()
            // :: Then, show the shrinking logo and play music theme
            this.starsVisible = true
            musicTheme.play()
            this.animateLogo()
            // :: Finally, scroll text 'till finished
            this.animateScrollingText()
        },
        // :: Animate intro text with fades and waiting times
        animateIntroText() {
            return new Promise(async (resolve, reject) => {
                this.introTextVisible = true
                await this.wait(4000)
                this.introTextVisible = false
                await this.wait(1500)
                return resolve()
            });
        },
        // :: Start shrinking the logo, hide when shrinking finishes
        async animateLogo(){
            this.logoVisible = true
            await this.wait(12000)
            this.logoVisible = false
        },
        // :: Animate scrolling text and stop everything when finished
        async animateScrollingText() {
            this.slidingTextVisible = true
            await this.wait(85000)
            this.slidingTextVisible = false
            this.starsVisible = false
            this.creditsVisible = true
            musicTheme.pause()
        }
    }
})